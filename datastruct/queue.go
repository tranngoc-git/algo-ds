package datastruct

type (
	queue struct {
		head, tail *queueNode
		length     int
	}
	queueNode struct {
		value interface{}
		next  *queueNode
	}
)

// newQueue create new nil queue
func newQueue() *queue {
	return &queue{
		head:   nil,
		tail:   nil,
		length: 0,
	}
}

func (q *queue) push(value interface{}) {
	newQ := &queueNode{
		value: value,
		next:  nil,
	}

	// empty queue
	if q.length == 0 {
		q.head = newQ
		q.tail = newQ
		q.length++
		return
	}

	q.tail.next = newQ
	q.tail = newQ
	q.length++

	return

}

func (q *queue) pop() interface{} {
	if q.length == 0 {
		return nil
	}

	rs := q.head.value
	if q.length == 1 {
		q.head = nil
		q.tail = nil

		q.length--
		return rs
	}

	q.head = q.head.next
	q.length--

	return rs
}

func (q *queue) peek() interface{} {
	if q.length == 0 {
		return nil
	}

	return q.head.value
}
