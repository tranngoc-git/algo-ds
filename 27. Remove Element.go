package main

import "fmt"

func removeElement(nums []int, val int) int {
	c := 0
	rs := []int{}

	for _, v := range nums {
		if v != val {
			rs = append(rs, v)
			c++
		}
	}
	for k := range rs {
		nums[k] = rs[k]
	}
	fmt.Println(nums)

	return c
}
