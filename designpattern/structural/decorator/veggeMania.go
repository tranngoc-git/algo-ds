package decorator

type veggeMania struct {
	pizza pizza
}

func (p *veggeMania) getPrice() int {
	return p.pizza.getPrice() + 3
}

func (p *veggeMania) getTopping() []string {
	return append(p.pizza.getTopping(), "vegge mania")
}

func withVeggeManiaTopping(pizza pizza) pizza {
	return &veggeMania{
		pizza: pizza,
	}
}
