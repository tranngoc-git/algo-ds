package main

import "strconv"

func solve202(n int) bool {
	s := make(map[int]struct{})
	for sum(n) != 1 {
		n = sum(n)
		if _, ok := s[n]; ok {
			return false
		}
		s[n] = struct{}{}
	}
	return true
}

func sum(n int) int {
	b := []byte(strconv.Itoa(n))
	rs := 0
	for _, v := range b {
		r, _ := strconv.Atoi(string(v))
		rs += r * r
	}
	return rs
}
