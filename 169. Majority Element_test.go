package main

import "testing"

func Test_solve169(t *testing.T) {
	type args struct {
		nums []int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Test 1",
			args: args{
				nums: []int{3, 2, 3},
			},
			want: 3,
		},
		{
			name: "Test 2",
			args: args{
				nums: []int{2, 2, 1, 1, 1, 2, 2},
			},
			want: 2,
		},
		{
			name: "Test 3",
			args: args{
				nums: []int{2},
			},
			want: 2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := solve169(tt.args.nums); got != tt.want {
				t.Errorf("solve169() = %v, want %v", got, tt.want)
			}
		})
	}
}
